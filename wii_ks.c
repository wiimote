/*
 * Wiimote Bluetooth mouse driver
 *
 * Kernelspace Part
 *
 * Copyright (C) 2009, 2010  Daniel Borkmann <daniel@netsniff-ng.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/slab.h>

#include <asm/uaccess.h>

#include "wii.h"

MODULE_DESCRIPTION("Wiimote Bluetooth mouse driver");
MODULE_AUTHOR("Daniel Borkmann <daniel@netsniff-ng.org>");
MODULE_LICENSE("GPL");

#define DEV_NAME        "wiimote"
#define DEV_PRINT_PRE   DEV_NAME ": "

#define jmp_err(var, errno, label)  \
	do {                        \
		var = (errno);      \
		goto label;         \
	} while(0)

/* Only one device at a time supported. */
static struct input_dev    *wiimote_dev = NULL;
static struct wiimote_data *wiimote_buf = NULL;

static inline void wiimote_update_sync(void)
{
	BUG_ON(!wiimote_dev);
	input_sync(wiimote_dev);
}

static inline uint16_t wiimote_get_translation_from_button(unsigned int button)
{
	size_t i, len = sizeof(wiimote_translation_table) / 
			(sizeof(unsigned int) << 1);

	/* Sure this is linear, but we only have max. eleven buttons and 
	 * stay flexible that way. */
	for(i = 0; i < len; ++i) {
		if(wiimote_translation_table[i][0] == button)
			return wiimote_translation_table[i][1];
	}

	return 0;
}

static inline void wiimote_update_input_event_key(unsigned int button)
{
	BUG_ON(!wiimote_dev || !wiimote_buf);

	input_event(wiimote_dev, EV_KEY,
		    wiimote_get_translation_from_button(button), 
		   (wiimote_buf->buttons & button) == button); 
}

static void wiimote_update_input_event_keys(void)
{
	size_t i, len = sizeof(wiimote_translation_table) / 
			(sizeof(unsigned int) << 1);

	BUG_ON(!wiimote_dev || !wiimote_buf);

	for(i = 0; i < len; ++i) {
		wiimote_update_input_event_key(wiimote_translation_table[i][0]);
		wiimote_update_sync();
	}
}

static inline void wiimote_update_input_event_vect(void)
{
	BUG_ON(!wiimote_dev || !wiimote_buf);

	input_event(wiimote_dev, EV_REL, REL_X, wiimote_buf->pos_dx);
	input_event(wiimote_dev, EV_REL, REL_Y, wiimote_buf->pos_dy);
}

static ssize_t wiimote_write(struct file *file, const char *buff, size_t len, 
			     loff_t *off)
{
	if(len != sizeof(*wiimote_buf)) {
		printk(KERN_ERR DEV_PRINT_PRE "Wrong data packet size!\n");
		return -EINVAL;
	}

	if(copy_from_user(wiimote_buf, buff, len)) {
		printk(KERN_ERR DEV_PRINT_PRE "Error while copying between "
					      "address spaces!\n");
		return -EIO;
	}

	wiimote_update_input_event_keys();
	wiimote_update_input_event_vect();

	return 0;
}

static void wiimote_register_event_keys(void)
{
	size_t i, len = sizeof(wiimote_translation_table) / 
			(sizeof(unsigned int) << 1);

	BUG_ON(!wiimote_dev);

	set_bit(EV_KEY, wiimote_dev->evbit);

	for(i = 0; i < len; ++i) {
		set_bit(wiimote_translation_table[i][1], wiimote_dev->keybit);
	}
}

static inline void wiimote_register_event_vect(void)
{
	BUG_ON(!wiimote_dev);

	set_bit(EV_REL, wiimote_dev->evbit);

	set_bit(REL_X, wiimote_dev->relbit);
	set_bit(REL_Y, wiimote_dev->relbit);
}

static inline void wiimote_setup(void)
{
	BUG_ON(!wiimote_dev);

	wiimote_dev->name = DEV_NAME;

	wiimote_dev->id.bustype = BUS_BLUETOOTH;
	wiimote_dev->id.vendor  = ID_VENDOR;
	wiimote_dev->id.product = ID_PRODUCT;
	wiimote_dev->id.version = ID_VERSION;
}

static int wiimote_open(struct inode *inode, struct file *file)
{
	int err, rc = 0;

	wiimote_dev = input_allocate_device();
	if(!wiimote_dev) {
		printk(KERN_ERR DEV_PRINT_PRE "Error during device "
					      "allocation\n");
		return -ENOMEM;
	}

	wiimote_setup();
	wiimote_register_event_vect();
	wiimote_register_event_keys();

	if((err = input_register_device(wiimote_dev))) {
		printk(KERN_ERR DEV_PRINT_PRE "Error registering device!\n");
		jmp_err(rc, err, out_1);
	}

	wiimote_buf = kmalloc(sizeof(*wiimote_buf), GFP_KERNEL);
	if(!wiimote_buf) {
		printk(KERN_ERR DEV_PRINT_PRE "Error allocating private device "
					      "data!\n");
		jmp_err(rc, -ENOMEM, out_2);
	}

	printk(KERN_INFO DEV_PRINT_PRE "opened\n");
	return 0;
out_2:
	input_unregister_device(wiimote_dev);
out_1:
	input_free_device(wiimote_dev);
	return rc;
}

static int wiimote_release(struct inode *inode, struct file *file)
{
	BUG_ON(!wiimote_dev);

	input_unregister_device(wiimote_dev);
	kfree(wiimote_buf);

	printk(KERN_INFO DEV_PRINT_PRE "released\n");
	return 0;
}

static struct file_operations wiimote_ops = {
	.open    = wiimote_open,
	.write   = wiimote_write,
	.release = wiimote_release,
};

static struct miscdevice wiimote_misc_dev = {
	.fops  = &wiimote_ops,
	.minor = MISC_DYNAMIC_MINOR,
	.name  = DEV_NAME,
};

int __init wiimote_init(void)
{
	int rc = misc_register(&wiimote_misc_dev);
	if(!rc)
		printk(KERN_INFO DEV_PRINT_PRE "Driver registered\n");
	return rc;
}

void __exit wiimote_cleanup(void)
{
	misc_deregister(&wiimote_misc_dev);
	printk(KERN_INFO DEV_PRINT_PRE "Driver unregistered\n");
}

module_init(wiimote_init);
module_exit(wiimote_cleanup);
