obj-m += wii_ks.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
	gcc -O2  -Wall -g -lbluetooth -lpthread -lm -o wiimote wii_us.c

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
	rm -f *.o wiimote Module.symvers modules.order
