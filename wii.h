/*
 * Wiimote Bluetooth mouse driver
 *
 * Macros & definitions
 *
 * Copyright (C) 2009, 2010  Daniel Borkmann <daniel@netsniff-ng.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __KERNEL__
# include <stdint.h>
#endif /* __KERNEL__ */

/* These are the Nintendo Wii button defs */
#define BUTTON_TWO           0x0001
#define BUTTON_ONE           0x0002
#define BUTTON_B             0x0004
#define BUTTON_A             0x0008
#define BUTTON_MINUS         0x0010
#define BUTTON_HOME          0x0080
#define BUTTON_LEFT          0x0100
#define BUTTON_RIGHT         0x0200
#define BUTTON_DOWN          0x0400
#define BUTTON_UP            0x0800
#define BUTTON_PLUS          0x1000

#define L2CAP_PSM_HIDP_CTRL  0x11
#define L2CAP_PSM_HIDP_INTR  0x13

#define WII_SET_CALIBRATION_DATA { 0x52, 0x17, 0x00, 0x00, 0x00, 0x16, 0x00, 0x07 }
#define WII_ENABLE_BUTTON_ACCELM { 0x52, 0x12, 0x00, 0x31 }
#define WII_ENABLE_RUMBLE        { 0x52, 0x11, 0x01 }
#define WII_DISABLE_RUMBLE       { 0x52, 0x11, 0x00 }
#define WII_GENERIC_LED          { 0x52, 0x11, 0x00 }

/* Wii axis inversion: 1.0 off | -1.0 on */
#define WII_INVERT_Y_AXIS            1.0f

#define WII_BLK_TRANSMISSION_LEN     7
#define WII_BLK_SLEEP_GAP_LEN      300

struct wiimote_data {
	uint16_t buttons;
	int32_t  pos_dx;
	int32_t  pos_dy;
};

#ifdef __KERNEL__
# include <linux/input.h>

/* Mapping can easily be modified by defs from linux/input.h */
static const unsigned int wiimote_translation_table[][2] = {
	{ BUTTON_B,     BTN_LEFT },
	{ BUTTON_A,     BTN_RIGHT },
	{ BUTTON_HOME,  BTN_MIDDLE },
	{ BUTTON_LEFT,  KEY_LEFT },
	{ BUTTON_RIGHT, KEY_RIGHT },
	{ BUTTON_UP,    KEY_UP },
	{ BUTTON_DOWN,  KEY_DOWN },
	{ BUTTON_ONE,   KEY_PAGEUP },
	{ BUTTON_TWO,   KEY_PAGEDOWN },
	{ BUTTON_PLUS,  KEY_VOLUMEUP },
	{ BUTTON_MINUS, KEY_VOLUMEDOWN },
};
#endif /* __KERNEL__ */
