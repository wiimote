/*
 * Wiimote Bluetooth mouse driver
 *
 * Userspace Part
 *
 * Copyright (C) 2009, 2010  Daniel Borkmann <daniel@netsniff-ng.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#define PROGNAME_STRING  "wiimote"
#define VERSION_STRING   "0.5.0"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <signal.h>
#include <getopt.h>
#include <ctype.h>
#include <assert.h>
#include <errno.h>
#include <pthread.h>

#include <sys/socket.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/l2cap.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/sdp.h>
#include <bluetooth/hidp.h>

#include "wii.h"

static volatile sig_atomic_t sigint = 0;

struct wiimote_ctl {
	char *address;
	char *device;
	int sock_ctrl;
	int sock_intr;
	int fd_device;
};

static struct option long_options[] = {
	{"addr",    required_argument, 0, 'a'},
	{"dev",     required_argument, 0, 'd'},
	{"scan",    no_argument,       0, 's'},
	{"version", no_argument,       0, 'v'},
	{"help",    no_argument,       0, 'h'},
	{0, 0, 0, 0}
};

/*
 *  taken from BlueZ
 */
static int l2cap_connect(bdaddr_t *src, bdaddr_t *dst, unsigned short psm)
{
	struct sockaddr_l2 addr;
	struct l2cap_options opts;
	int sk;

	if ((sk = socket(PF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP)) < 0)
		return -1;

	memset(&addr, 0, sizeof(addr));
	addr.l2_family  = AF_BLUETOOTH;
	bacpy(&addr.l2_bdaddr, src);

	if (bind(sk, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		close(sk);
		return -1;
	}

	memset(&opts, 0, sizeof(opts));
	opts.imtu = HIDP_DEFAULT_MTU;
	opts.omtu = HIDP_DEFAULT_MTU;
	opts.flush_to = 0xffff;

	setsockopt(sk, SOL_L2CAP, L2CAP_OPTIONS, &opts, sizeof(opts));

	memset(&addr, 0, sizeof(addr));
	addr.l2_family  = AF_BLUETOOTH;
	bacpy(&addr.l2_bdaddr, dst);
	addr.l2_psm = htobs(psm);

	if (connect(sk, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		close(sk);
		return -1;
	}

	return sk;
}

static void version(void)
{
	printf("%s %s\n", PROGNAME_STRING, VERSION_STRING);
	exit(EXIT_SUCCESS);
}

static void help(void)
{
	printf("%s %s\n\n", PROGNAME_STRING, VERSION_STRING);
	printf("Usage: wiimote [options]\n\n");
	printf("  -a|--addr <arg>   BT address to connect to\n");
	printf("  -d|--dev <arg>    Wiimote device file (e.g. /dev/wiimote)\n");
	printf("  -s|--scan         Scan for Wiimote devices\n");
	printf("  -v|--version      Prints out version\n");
	printf("  -h|--help         Prints out this help\n\n");
        printf("Please report bugs to <daniel@netsniff-ng.org>\n");
        printf("Copyright (C) 2009, 2010 Daniel Borkmann\n");
        printf("License: GNU GPL version 2\n");
        printf("This is free software: you are free to change and "
               "redistribute it.\n");
        printf("There is NO WARRANTY, to the extent permitted by law.\n");
	exit(EXIT_SUCCESS);
}

static inline void init_configuration(struct wiimote_ctl *wctl)
{
	assert(wctl);
	memset(wctl, 0, sizeof(*wctl));
}

static void clean_configuration(struct wiimote_ctl *wctl)
{
	assert(wctl);
	free(wctl->address);
	free(wctl->device);
}

static void wiimote_scan_devices(void)
{
	int i;

	int device_id, sock, len = 8, flags;
	int max_resp = 255, num_resp;

	char address[19] = { 0 };
	char name[248]   = { 0 };

	inquiry_info *ii = NULL;
    
	printf("Scanning for Wiimote devices ...\n");
	printf("Set your Wiimote into discoverable mode (press 1 + 2).\n");

	flags = IREQ_CACHE_FLUSH;

	device_id = hci_get_route(NULL);

	sock = hci_open_dev(device_id);
	if(device_id < 0 || sock < 0) {
		perror("hci_open_dev");
		exit(EXIT_FAILURE);
	}

	ii = malloc(max_resp * sizeof(*ii));
	if(!ii) {
		perror("malloc");
		exit(EXIT_FAILURE);
	}

	num_resp = hci_inquiry(device_id, len, max_resp, NULL, &ii, flags);
	if(num_resp < 0) 
		perror("hci_inquiry");

	for(i = 0; i < num_resp; ++i) {
		ba2str(&(ii + i)->bdaddr, address);
		memset(name, 0, sizeof(name));

		if(hci_read_remote_name(sock, &(ii + i)->bdaddr, sizeof(name), 
		   name, 0) < 0) {
			strcpy(name, "[unknown]");
		}

		if(strstr(name, "Nintendo") != NULL)
			printf("%s -- %s\n", address, name);
	}

	free(ii);
	close(sock);
}

static void set_configuration(int argc, char **argv, struct wiimote_ctl *wctl)
{
	int c, sl, opt_idx;

	assert(argv);
	assert(wctl);

	while ((c = getopt_long(argc, argv, "a:d:svh", long_options, 
				&opt_idx)) != EOF) {
		switch (c) {
		case 'h':
			help();
			break;
		case 'v':
			version();
			break;
		case 'a':
			/* Allowed: XX:XX:XX:XX:XX:XX */
			sl = strlen(optarg);
			if(sl != 17) {
				fprintf(stderr, "Wrong address format!\n");
				help();
			}

			wctl->address = strdup(optarg);
			if(!wctl->address) {
				perror("strdup");
				help();
			}
			break;
		case 'd':
			wctl->device = strdup(optarg);
			if(!wctl->device) {
				perror("strdup");
				help();
			}
			break;
		case 's':
			wiimote_scan_devices();
			exit(EXIT_SUCCESS);
			break;
		case '?':
			switch (optopt) {
			case 'a':
			case 'd':
				printf("Option -%c requires an argument!\n", 
				       optopt);
				help();
			default:
				if(isprint(optopt)) {
					printf("Unknown option character "
					       "`0x%X\'!\n", optopt);
				}
				help();
			}

			return;
		default:
			abort();
		}
	}
}

static void check_configuration(struct wiimote_ctl *wctl)
{
	assert(wctl);
	if(!wctl->address || !wctl->device) {
		clean_configuration(wctl);
		help();
	}
}

static void signal_action(int s)
{
	sigint = (s == SIGINT);
}

static void set_signalhandler(void)
{
	sigset_t block_mask;
	struct sigaction usr_action;	

	sigfillset(&block_mask);
	usr_action.sa_handler = signal_action;
	usr_action.sa_mask = block_mask;
	usr_action.sa_flags = 0;
	sigaction(SIGINT, &usr_action, NULL);
}

static int wiimote_init(struct wiimote_ctl *wctl)
{
	assert(wctl);

	wctl->fd_device = open(wctl->device, O_RDWR);
	if(wctl->fd_device < 0) {
		perror("open");
		exit(EXIT_FAILURE);
	}

	wctl->sock_ctrl = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);
	wctl->sock_intr = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);

	if(wctl->sock_ctrl < 0 || wctl->sock_intr < 0) {
		perror("socket");
		exit(EXIT_FAILURE);
	}

	return 0;
}

static void wiimote_cleanup(struct wiimote_ctl *wctl)
{
	close(wctl->fd_device);
	close(wctl->sock_ctrl);
	close(wctl->sock_intr);
}

static int wiimote_inject_config_data(struct wiimote_ctl *wctl)
{
	int rc;
	uint8_t calibration_data[] = WII_SET_CALIBRATION_DATA;
	uint8_t enable_ba_data[]   = WII_ENABLE_BUTTON_ACCELM;

	assert(wctl);

	rc = write(wctl->sock_ctrl, calibration_data, sizeof(calibration_data));
	if(rc < 0) {
		perror("write");
		return rc;
	}

	rc = write(wctl->sock_ctrl, enable_ba_data, sizeof(enable_ba_data));
	if(rc < 0) {
		perror("write");
		return rc;
	}

	return 0;
}

static int wiimote_connect_intr_channel(struct wiimote_ctl *wctl)
{
	struct sockaddr_l2 addr_intr;

	assert(wctl);

	addr_intr.l2_family = AF_BLUETOOTH;
	addr_intr.l2_psm    = htobs(L2CAP_PSM_HIDP_INTR);

	str2ba(wctl->address, &addr_intr.l2_bdaddr);
	
//	return connect(wctl->sock_intr, (struct sockaddr *) &addr_intr, 
//		       sizeof(addr_intr));
	errno = 0;
	wctl->sock_intr = l2cap_connect(BDADDR_ANY, &addr_intr.l2_bdaddr, L2CAP_PSM_HIDP_INTR);
	perror("l2cap_connect intr");
	return (wctl->sock_intr < 0);
}

static int wiimote_connect_ctrl_channel(struct wiimote_ctl *wctl)
{
	struct sockaddr_l2 addr_ctrl;

	assert(wctl);

	addr_ctrl.l2_family = AF_BLUETOOTH;
	addr_ctrl.l2_psm    = htobs(L2CAP_PSM_HIDP_CTRL);

	str2ba(wctl->address, &addr_ctrl.l2_bdaddr);
	
//	return connect(wctl->sock_ctrl, (struct sockaddr *) &addr_ctrl, 
//		       sizeof(addr_ctrl));
	errno = 0;
	wctl->sock_ctrl = l2cap_connect(BDADDR_ANY, &addr_ctrl.l2_bdaddr, L2CAP_PSM_HIDP_CTRL);
	perror("l2cap_connect intr");
	return (wctl->sock_ctrl < 0);
}

static int wiimote_connect_channels(struct wiimote_ctl *wctl)
{
	int rc = 0;

	assert(wctl);

	rc = wiimote_connect_ctrl_channel(wctl);
	if(rc) { perror("ctrl channel");
		return -EIO; }
	rc = wiimote_connect_intr_channel(wctl);
	if(rc) { perror("intr channel");
		return -EIO; }
	rc = wiimote_inject_config_data(wctl);
	if(rc) { perror("conf data");
		return rc; }
	return 0;
}

static int wiimote_leds(struct wiimote_ctl *wctl, int l1, int l2, int l3, int l4)
{
	int rc;
	uint8_t enable_leds[] = WII_GENERIC_LED;

	assert(wctl);

	if(l1)
		enable_leds[2] |= 0x10;
	if(l2)
		enable_leds[2] |= 0x20;
	if(l3)
		enable_leds[2] |= 0x40;
	if(l4)
		enable_leds[2] |= 0x80;

	rc = write(wctl->sock_ctrl, enable_leds, sizeof(enable_leds));
	if(rc < 0) {
		perror("write");
		return rc;
	}

	return 0;
}

static int wiimote_rumble(struct wiimote_ctl *wctl, uint32_t usec)
{
	int rc;
	uint8_t enable_rumble[]  = WII_ENABLE_RUMBLE;
	uint8_t disable_rumble[] = WII_DISABLE_RUMBLE;

	assert(wctl);
	
	rc = write(wctl->sock_ctrl, enable_rumble, sizeof(enable_rumble));
	if(rc < 0) {
		perror("write");
		return rc;
	}

	usleep(usec);

	rc = write(wctl->sock_ctrl, disable_rumble, sizeof(disable_rumble));
	if(rc < 0) {
		perror("write");
		return rc;
	}

	return 0;
}

static void *wiimote_rumble_thread(void *arg)
{
	struct wiimote_ctl *wctl = (struct wiimote_ctl *) arg;
	
	while(!sigint) {
		wiimote_leds(wctl, 1, 0, 0, 0);
		usleep(250000);
		wiimote_leds(wctl, 0, 1, 0, 0);
		usleep(250000);
		wiimote_leds(wctl, 0, 0, 1, 0);
		usleep(250000);
		wiimote_leds(wctl, 0, 0, 0, 1);
		usleep(250000);
		wiimote_leds(wctl, 0, 0, 1, 0);
		usleep(250000);
		wiimote_leds(wctl, 0, 1, 0, 0);
		usleep(250000);
		wiimote_leds(wctl, 1, 0, 0, 0);
		usleep(250000);
	}

	pthread_exit(0);
}

static int wiimote_read_sensors_loop(struct wiimote_ctl *wctl)
{
	int rc;

	float norm;
	float calx, caly, calz;
	float dx, dy, dz;
	float roll, pitch;
	
	uint8_t buffer[WII_BLK_TRANSMISSION_LEN];
	struct wiimote_data wbuf;

	assert(wctl);

	/* Initial settings */
	calx = 122.f;
	caly = 129.f;
	calz = 123.f;

	while(!sigint) {
		usleep(WII_BLK_SLEEP_GAP_LEN);

		memset(buffer, 0, sizeof(buffer));
		
		rc = read(wctl->sock_intr, buffer, sizeof(buffer));
		if(rc < 0) {
			perror("read");
			return rc;
		}
		
		/* Check for valid buffer */
		if(buffer[0] != 0xA1 && buffer[1] != 0x33)
			continue;

		wbuf.buttons = ((uint16_t) ((buffer[2] & ~0x60) << 8) & 0xFF00) | 
			       ((uint16_t)  (buffer[3] & ~0x60) & 0x00FF);

		/* Some bits are also encoded between button status bits */
		dx = 1.0f * (buffer[4] | (buffer[2] & 0x60) >> 5) - calx; 
		dy = 1.0f * (buffer[5] | (buffer[3] & 0x20) >> 5) - caly; 
		dz = 1.0f * (buffer[6] | (buffer[3] & 0x40) >> 6) - calz;

		norm = 1.f / sqrtf(dx * dx + dy * dy + dz * dz);

		dx = 2.0f * atanf(dx * norm);
		dy = 2.0f * atanf(dy * norm);

		roll  = dx * 180.0f / M_PI;
		pitch = dy * 180.0f / M_PI;

		wbuf.pos_dx = 0;
		if((int) roll > 30 || (int) roll < -30)
			wbuf.pos_dx = (int) roll / 20;
		
		wbuf.pos_dy = 0;
		if((int) pitch > 30 || (int) pitch < -30)
			wbuf.pos_dy = (int) pitch / 20;

		wbuf.pos_dy *= WII_INVERT_Y_AXIS;
		
		rc = write(wctl->fd_device, &wbuf, sizeof(wbuf));
		if(rc < 0){
			perror("write");
			return rc;
		}		
	}

	return 0;
}

int main(int argc, char **argv)
{
	int rc;
	pthread_t rumble_thread;
	struct wiimote_ctl  wctl;

	init_configuration(&wctl);
	set_configuration(argc, argv, &wctl);
	check_configuration(&wctl);
	set_signalhandler();

	printf("Set your Wiimote (%s) into discoverable mode (press 1 + 2).\n",
	       wctl.address);
	
	wiimote_init(&wctl);
	while(wiimote_connect_channels(&wctl) != 0 && !sigint)
		/* NOP */ ;
	wiimote_rumble(&wctl, 2000000);

	rc = pthread_create(&rumble_thread, NULL, wiimote_rumble_thread, &wctl);
        if(rc) {
                perror("pthread_create");
                exit(EXIT_FAILURE);
        }

	wiimote_read_sensors_loop(&wctl);
	wiimote_cleanup(&wctl);

	clean_configuration(&wctl);
	return 0;
}
